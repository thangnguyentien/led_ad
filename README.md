## Led_ad
-   Dự án bảng Led quảng cáo siêu thị, sản xuất các bản demo

## Description
-   Project này chỉ sử dụng ở mức Demo
-   Demo 1: Arduino Pro Micro, cảm biến siêu âm HSR-05
-   Demo 2: ESP32, cảm biến laser VL31

## Badges

## Visual

## Installation

## Usage
-   Demo 1: Đọc cảm biến và xuất khoảng cách ra Serial
-   Demo 2: Cùng công dụng nhưng tăng độ chính xác
