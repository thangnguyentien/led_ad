/* Product by Thang Nguyen */
/* Code doc 4 cam bien HR05
    Chuc nang: phat hien vat the va xuat ra chuoi du lieu duoc cai dat truoc
  */


/*Cấu hình chân Sensor 1*/
const int trig_1 = 2;     // chân trig của HC-SR04
const int echo_1 = 3;     // chân echo của HC-SR04
/*Cấu hình chân Sensor 2*/
const int trig_2 = 4;     // chân trig của HC-SR04
const int echo_2 = 5;     // chân echo của HC-SR04
/*Cấu hình chân Sensor 3*/
const int trig_3 = 6;     // chân trig của HC-SR04
const int echo_3 = 7;     // chân echo của HC-SR04
/*Cấu hình chân Sensor 4*/
const int trig_4 = 8;     // chân trig của HC-SR04
const int echo_4 = 9;     // chân echo của HC-SR04

char message[21] = {0x7B, 0x22, 0x6D, 0x22, 0x3A, 0x31, 0x2C, 0x22,
                    0x74, 0x22, 0x3A, 0x33, 0x2C, 0x22, 0x73, 0x22,
                    0x3A, 0x32, 0x7D, 0x0D, 0x0A};

void setup()
{
    Serial.begin(9600);     // giao tiếp Serial với baudrate 9600
    pinMode(trig_1,OUTPUT);   // chân trig sẽ phát tín hiệu
    pinMode(echo_1,INPUT);    // chân echo sẽ nhận tín hiệu

    pinMode(trig_2,OUTPUT);   // chân trig sẽ phát tín hiệu
    pinMode(echo_2,INPUT);    // chân echo sẽ nhận tín hiệu

    pinMode(trig_3,OUTPUT);   // chân trig sẽ phát tín hiệu
    pinMode(echo_3,INPUT);    // chân echo sẽ nhận tín hiệu

    pinMode(trig_4,OUTPUT);   // chân trig sẽ phát tín hiệu
    pinMode(echo_4,INPUT);    // chân echo sẽ nhận tín hiệu
}
 
void loop()
{
    unsigned long duration_1,duration_2,duration_3,duration_4; // biến đo thời gian
    int distance_1,distance_2,distance_3,distance_4;           // biến lưu khoảng cách
    
    /* Phát xung từ chân trig */
    digitalWrite(trig_1,0);   // tắt chân trig
    delayMicroseconds(2);
    digitalWrite(trig_1,1);   // phát xung từ chân trig
    delayMicroseconds(5);   // xung có độ dài 5 microSeconds
    digitalWrite(trig_1,0);   // tắt chân trig
    
    /* Tính toán thời gian */
    // Đo độ rộng xung HIGH ở chân echo. 
    duration_1 = pulseIn(echo_1,HIGH);  
    // Tính khoảng cách đến vật.
    distance_1 = int(duration_1/2/29.412);
    
    if((50 < distance_1)&&(distance_1 < 300))
    {
    /* In kết quả ra Serial Monitor */
    Serial.print("Sensor 1 : ");
    Serial.print(distance_1);
    Serial.println("cm");
    }
    delay(100);

    /* Phát xung từ chân trig */
    digitalWrite(trig_2,0);   // tắt chân trig
    delayMicroseconds(2);
    digitalWrite(trig_2,1);   // phát xung từ chân trig
    delayMicroseconds(5);   // xung có độ dài 5 microSeconds
    digitalWrite(trig_2,0);   // tắt chân trig
    
    /* Tính toán thời gian */
    // Đo độ rộng xung HIGH ở chân echo. 
    duration_2 = pulseIn(echo_2,HIGH);  
    // Tính khoảng cách đến vật.
    distance_2 = int(duration_2/2/29.412);
    
    if((50 < distance_2)&&(distance_2 < 300))
    {
    /* In kết quả ra Serial Monitor */
    Serial.print("Sensor 2 : ");
    Serial.print(distance_2);
    Serial.println("cm");
    }
    delay(100);

    /* Phát xung từ chân trig */
    digitalWrite(trig_3,0);   // tắt chân trig
    delayMicroseconds(2);
    digitalWrite(trig_3,1);   // phát xung từ chân trig
    delayMicroseconds(5);   // xung có độ dài 5 microSeconds
    digitalWrite(trig_3,0);   // tắt chân trig
    
    /* Tính toán thời gian */
    // Đo độ rộng xung HIGH ở chân echo. 
    duration_3 = pulseIn(echo_3,HIGH);  
    // Tính khoảng cách đến vật.
    distance_3 = int(duration_3/2/29.412);
    
    if((275 < distance_3)&&(distance_3 < 325))
    {
    /* In kết quả ra Serial Monitor */
    Serial.print("Sensor 3 : ");
    Serial.print(distance_3);
    Serial.println("cm");
    }
    delay(100);

    /* Phát xung từ chân trig */
    digitalWrite(trig_4,0);   // tắt chân trig
    delayMicroseconds(2);
    digitalWrite(trig_4,1);   // phát xung từ chân trig
    delayMicroseconds(5);   // xung có độ dài 5 microSeconds
    digitalWrite(trig_4,0);   // tắt chân trig
    
    /* Tính toán thời gian */
    // Đo độ rộng xung HIGH ở chân echo. 
    duration_4 = pulseIn(echo_4,HIGH);  
    // Tính khoảng cách đến vật.
    distance_4 = int(duration_4/2/29.412);
    
    if((275 < distance_4)&&(distance_4 < 325))
    {
    /* In kết quả ra Serial Monitor */
    Serial.print("Sensor 4 : ");
    Serial.print(distance_4);
    Serial.println("cm");
    }
    delay(100);

}







